/** Object contains all valid movement types for MovableItem map's
 * @name MOVES
 * @constant {object}
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export const MOVES = {
  /** All of MOVES
   * @enum
   */
  ALL: 0b111111,

  /** None of MOVES
   * @enum
   */
  NOT: 0b000000,

  /** Move by Walk (empty, friend, enemy)
   * @enum
   */
  WALK: 0b000111,

  /** Move by Walk if target is empty
   * @enum
   */
  WALK_EMPTY: 0b000001,

  /** Move by Walk if target is friend
   * @enum
   */
  WALK_FRIEND: 0b000010,

  /** Move by Walk if target is enemy
   * @enum
   */
  WALK_ENEMY: 0b000100,

  /** Move by Jump (empty, friend, enemy)
   * @enum
   */
  JUMP: 0b111000,

  /** Move by Jump if target is empty
   * @enum
   */
  JUMP_EMPTY: 0b001000,

  /** Move by Jump if target is friend
   * @enum
   */
  JUMP_FRIEND: 0b010000,

  /** Move by Jump if target is enemy
   * @enum
   */
  JUMP_ENEMY: 0b100000
}

/** Object contains all valid directions for MovableItem map's
 * @name DIRECTIONS
 * @constant {object}
 * @memberof module:Boardgame
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export const DIRECTIONS = {
  /** Horizontal and Vertical and Diagonal
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  ALL: 0b11111111,

  /** None of DIRECTIONS
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  NOT: 0b00000000,

  /** Horizontal and Vertical
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  HORIZONTAL_VERTICAL: 0b00001111,
  HV: 0b00001111,

  /** West and East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  HORIZONTAL: 0b00001100,
  H: 0b00001100,

  /** North and South
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  VERTICAL: 0b00000011,
  V: 0b00000011,

  /** North
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  NORTH: 0b00000001,
  N: 0b00000001,

  /** South
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  SOUTH: 0b00000010,
  S: 0b00000010,

  /** West
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  WEST: 0b00000100,
  W: 0b00000100,

  /** East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  EAST: 0b00001000,
  E: 0b00001000,

  /** North-West and North-East and South-West and South-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  DIAGONAL: 0b11110000,
  D: 0b11110000,

  /** North-West and North-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  DIAGONAL_NORTH: 0b00110000,
  DN: 0b00110000,

  /** South-West and South-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  DIAGONAL_SOUTH: 0b11000000,
  DS: 0b11000000,

  /** North-West and South-West
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  DIAGONAL_WEST: 0b01010000,
  DW: 0b01010000,

  /** North-East and South-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  DIAGONAL_EAST: 0b10100000,
  DE: 0b10100000,

  /** North-West
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  NORTH_WEST: 0b00010000,
  NW: 0b00010000,

  /** North-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  NORTH_EAST: 0b00100000,
  NE: 0b00100000,

  /** South-West
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  SOUTH_WEST: 0b01000000,
  SW: 0b01000000,

  /** South-East
   * @enum
   * @memberof module:Boardgame.DIRECTIONS
   */
  SOUTH_EAST: 0b10000000,
  SE: 0b10000000
}
