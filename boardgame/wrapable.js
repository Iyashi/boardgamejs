/** Class adds events and a wrapper reference to derived classes
 * @todo improve provisional event system
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class Wrapable {
  /** Create wrapable game object
   * @constructor
   */
  constructor () {
    this._initialized = false
    this._destroyed = false
    this._events = {
      init: [],
      destroy: []
    }
  }

  /** Trigger callbacks for given event name until a callback returns false
   * @param {string} name Event name
   * @param {object} params Arguments for callbacks
   * @return {bool} false to stop, true to continue with next callback of {name}
   */
  trigger (name, params = {}) {
    if (typeof name !== 'string') throw TypeError(`${this.constructor.name}: name not String`)
    if (!(name in this.events)) return false // throw Error(`${this.constructor.name}: doesnt contain name: ${name}`)
    params.source = this
    for (let callback of this.events[name]) {
      if (!callback(params)) return false
    }
    if (name === 'init') {
      this._initialized = true
      this._destroyed = false
    } else if (name === 'destroy') {
      this._initialized = false
      this._destroyed = true
    } else console.info(`${this.constructor.name} triggered ${name} with:`, params)
    return true
  }
  /** Trigger given callback for given event name
   * @param {string} name Event name
   * @param {callback} callback Callback to trigger
   * @param {object} params Arguments for callbacks
   * @return {bool} Callback return value
   */
  triggerSingle (name, callback, params = {}) {
    params.source = this
    if (name !== 'init' && name !== 'destroy') {
      console.info(`${this.constructor.name} triggered single ${name} with:`, params)
    }
    return callback(params)
  }

  /** Add callback for given event name
   * @param {string} name Event name
   * @param {callback} callback Event Callback
   */
  on (name, callback) { this.addListener(name, callback) }

  /** Add callback for given event name (alias of {@link Wrapable#on})
   * @param {string} name Event name
   * @param {callback} callback Event Callback
   */
  addListener (name, callback) {
    if (typeof name !== 'string') throw TypeError(`${this.constructor.name}: name not String`)
    if (!Array.isArray(this.events[name])) this.events[name] = []
    this.events[name].push(callback)
    if (this._initialized && name === 'init') this.triggerSingle(name, callback)
    if (this._destroyed && name === 'destroy') this.triggerSingle(name, callback)
  }

  /** get object of events
   * @type {object}
   * @description add new events by {@link Wrapable#on}
   */
  get events () { return this._events }
}
