import Item from './item'
import { MOVES, DIRECTIONS } from './constants'

/** Class represents a non-static Board Item
 * @extends Item
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class MovableItem extends Item {
  /** Create non-static Board Item with given movement map
   * @constructor
   * @param {array} map movement map Array<Array<MOVES>>
   */
  constructor (map) {
    super()
    this._steps = []
    this._map = map
  }

  /** Move item to given position
   * @param {number} x x position (> 0)
   * @param {number} y y position (> 0)
   */
  move (x, y) {
    this._x = x
    this._y = y
    this.steps.push({ x, y })
    this.trigger('move', { x, y })
  }

  /** set given directions of movement map to given value
   * @param {number} directions one or more of DIRECTIONS
   * @param {number} value one or more of MOVES
   * @param {number} count count of units from map center
   */
  setMapDirections (directions, value, count) {
    const h = (this.map.length - 1) / 2
    if (typeof count === 'undefined') count = h
    else count = Number(count)
    if (count <= 0 || count > h) throw Error(`${this.constructor.name}: count not Number<1-${h}>`)
    if (directions & DIRECTIONS.W) for (let i = 1; i <= count; i++) this.map[h][h - i] = value
    if (directions & DIRECTIONS.E) for (let i = 1; i <= count; i++) this.map[h][h + i] = value
    if (directions & DIRECTIONS.N) for (let i = 1; i <= count; i++) this.map[h - i][h] = value
    if (directions & DIRECTIONS.S) for (let i = 1; i <= count; i++) this.map[h + i][h] = value
    if (directions & DIRECTIONS.NW) for (let i = 1; i <= count; i++) this.map[h - i][h - i] = value
    if (directions & DIRECTIONS.NE) for (let i = 1; i <= count; i++) this.map[h - i][h + i] = value
    if (directions & DIRECTIONS.SW) for (let i = 1; i <= count; i++) this.map[h + i][h - i] = value
    if (directions & DIRECTIONS.SE) for (let i = 1; i <= count; i++) this.map[h + i][h + i] = value
  }

  /** get if item has already moved
   * @type {boolean}
   */
  get hasMoved () { return this.steps.length > 0 }

  /** get moved steps of Item
   * @type {array}
   */
  get steps () { return this._steps }

  /** get and set movement map of Item
   * @type {array}
   */
  get map () { return this._map }
  set map (map) {
    if (!Array.isArray(map)) throw TypeError(`${this.constructor.name}: map not Array<Array<0-${MOVES.ALL}>>`)
    for (let x of map) {
      if (!Array.isArray(x)) throw TypeError(`${this.constructor.name}: map not Array<Array<0-${MOVES.ALL}>>`)
      for (let v of x) {
        if (typeof v !== 'number' || v < MOVES.NOT || v > MOVES.ALL) throw TypeError(`${this.constructor.name}: map not Array<Array<0-${MOVES.ALL}>>`)
      }
    }
    this._map = map
  }

  /** get and set x and y position (> 0, > 0) of Item
   * @type {object}
   */
  get position () { return { x: this.x, y: this.y } }
  set position (v) {
    if (typeof v !== 'object') throw TypeError(`${this.constructor.name}: position not Object`)
    this.move(v.x, v.y)
  }

  /** get and set x position (> 0) of Item
   * @type {number}
   */
  get x () { return this._x }
  set x (v) {
    if (typeof v !== 'number' || v <= 0) throw TypeError(`${this.constructor.name}: x not Number > 0`)
    this.move(v, this.y)
  }

  /** get and set y position (> 0) of Item
   * @type {number}
   */
  get y () { return this._y }
  set y (v) {
    if (typeof v !== 'number' || v <= 0) throw TypeError(`${this.constructor.name}: y not Number > 0`)
    this.move(this.x, v)
  }
}
