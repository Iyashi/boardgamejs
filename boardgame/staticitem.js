import Item from './item'

/** Class represents a static Board Item
 * @extends Item
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class StaticItem extends Item {
  /** Create static Board Item at given position
   * @constructor
   * @param {number} x x position (> 0)
   * @param {number} y y position (> 0)
   */
  constructor (x, y) {
    super()
    if (typeof x !== 'number' || x <= 0) throw TypeError(`${this.constructor.name}: x not Number > 0`)
    this._x = x
    if (typeof y !== 'number' || y <= 0) throw TypeError(`${this.constructor.name}: y not Number > 0`)
    this._y = y
  }
}
