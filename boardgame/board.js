import Wrapable from './wrapable'
import Item from './item'
import MovableItem from './movableitem'
import { MOVES, DIRECTIONS } from './constants'

/** Class represents a Board
 * @extends Wrapable
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class Board extends Wrapable {
  /** Create Board
   * @constructor
   * @param {number} x x size (> 0)
   * @param {number} y y size (> 0)
   */
  constructor (x, y) {
    super()
    this._items = []
    if (typeof x !== 'number' || x <= 0) throw TypeError(`${this.constructor.name}: x not Number > 0`)
    this._x = x
    if (typeof y !== 'number' || y <= 0) throw TypeError(`${this.constructor.name}: y not Number > 0`)
    this._y = y
  }

  /** Init Board */
  init () { this.trigger('init') }

  /** Destroy Board */
  destroy () {
    const items = [...this.items]
    for (let item of items) {
      this.remove(item)
      item.destroy()
    }
    this.trigger('destroy')
  }

  /** Add new Items to Board
   * @param {...Item} items Items to add
   */
  add (...items) {
    for (let item of items) {
      if (!(item instanceof Item)) {
        console.warn(`${this.constructor.name}: Not Item:`, item)
        continue
      }
      if (this.items.includes(item)) {
        console.warn(`${this.constructor.name}: Already contains:`, item)
        continue
      }
      this.items.push(item)
      this.trigger('add', { target: item })
    }
  }

  /** Remove Items from Board
   * @param {...Item} items Items to remove
   */
  remove (...items) {
    const temp = [...items]
    for (let item of temp) {
      if (!(item instanceof Item)) {
        console.warn(`${this.constructor.name}: Not Item:`, item)
        continue
      }
      const idx = this.items.indexOf(item)
      if (idx === -1) {
        console.warn(`${this.constructor.name}: Doesnt contain:`, item)
        continue
      }
      this.items.splice(idx, 1)
      this.trigger('remove', { target: item })
    }
  }

  /** Generate all paths from Item's map
   * @param {MovableItem} item Item to use
   * @param {bool} [validate=true] Calls Board.validatePath
   * @return {array<Path>} Array of Path's (can be validated)
   */
  getPaths (item, validate = true) {
    if (!(item instanceof MovableItem)) throw TypeError('Not MovableItem')
    const paths = []
    const map = item.currentMap
    for (let y = 0; y < map.length; y++) {
      for (let x = 0; x < map[y].length; x++) {
        if (map[y][x] !== MOVES.NOT) paths.push(this.getPath(item, x + 1, y + 1, validate))
      }
    }
    return paths
  }

  /** Generate path to target x and y position (> 0, > 0) from Item's map
   * @param {MovableItem} item Item to use
   * @param {number} targetX Target x position (> 0)
   * @param {number} targetY Target y position (> 0)
   * @param {bool} [validate=true] Calls Board.validatePath
   * @return {Path} Path to target (can be validated)
   */
  getPath (item, targetX, targetY, validate = true) {
    if (!(item instanceof MovableItem)) throw TypeError('Not MovableItem')
    const path = { direction: DIRECTIONS.NOT, item, steps: [] }
    const map = item.currentMap
    const tx = targetX - item.x
    const ty = targetY - item.y
    const dx = Math.abs(tx)
    const dy = Math.abs(ty)
    const up = item.y > targetY
    const left = item.x > targetX
    if (item.x !== targetX && item.y === targetY) {
      path.direction = DIRECTIONS.HORIZONTAL
      for (let i = item.x; left ? i >= targetX : i <= targetX; left ? i-- : i++) {
        if (i === item.x) continue
        path.steps.push({ items: this.getItems(i, targetY), map: map[item.y - 1][i - 1] })
      }
    } else if (item.x === targetX && item.y !== targetY) {
      path.direction = DIRECTIONS.VERTICAL
      for (let i = item.y; up ? i >= targetY : i <= targetY; up ? i-- : i++) {
        if (i === item.y) continue
        path.steps.push({ items: this.getItems(targetX, i), map: map[i - 1][item.x - 1] })
      }
    } else if (dx === dy) {
      path.direction = DIRECTIONS.DIAGONAL
      for (let iy = item.y, ix = item.x; up ? iy >= targetY : iy <= targetY; up ? iy-- : iy++, left ? ix-- : ix++) {
        if (ix === item.x || iy === item.y) continue
        path.steps.push({ items: this.getItems(ix, iy), map: map[iy - 1][ix - 1] })
      }
    } else path.steps.push({ items: this.getItems(targetX, targetY), map: item.map[this.y - 1 + ty][this.x - 1 + tx] })
    return validate ? this.validatePath(path) : path
  }

  /**
   * Validate generated path (collision detection for {MOVES.WALK})
   * @param {Path} path Path to validate. (Path from Board.getPath)
   * @return {Path} Validated path
   */
  validatePath (path) {
    let valid = true
    let attack
    if (!path.steps.length) { path.valid = false; return path }
    if (path.direction === 'jump') {
      if (path.steps[0].map & MOVES.JUMP) {
        if (path.steps[0].items.length > 1) {
          if (!(path.steps[0].map & MOVES.JUMP_ENEMY) && path.item.player !== path.steps[0].items[1].player) valid = false
          if (!(path.steps[0].map & MOVES.JUMP_FRIEND) && path.item.player === path.steps[0].items[1].player) valid = false
        } else if (!(path.steps[0].map & MOVES.JUMP_EMPTY)) valid = false
      } else valid = false
      path.steps[0].valid = valid
    } else {
      for (let step of path.steps) {
        if (valid && step.map & MOVES.WALK) {
          if (step.items.length > 1) {
            if (!(step.map & MOVES.WALK_ENEMY) && path.item.player !== step.items[1].player) valid = false
            if (!attack && (step.map & MOVES.WALK_ENEMY) && path.item.player !== step.items[1].player) attack = step
            if (!(step.map & MOVES.WALK_FRIEND) && path.item.player === step.items[1].player) valid = false
          } else if (!(step.map & MOVES.WALK_EMPTY)) valid = false
        } else valid = false
        step.valid = valid
      }
      attack && path.steps.slice(path.steps.indexOf(attack) + 1).forEach($ => { $.valid = false })
    }
    path.valid = path.steps[path.steps.length - 1].valid
    return path
  }

  /** Returns all items at given position
   * @param {number} x x position
   * @param {number} y y position
   * @return {array<Item>} Items at given position
   */
  getItems (x, y) { return this.items.filter(item => item.x === x && item.y === y) }

  /** get Items on the Board
   * @member
   * @type {array<Item>}
   */
  get items () { return this._items }

  /** get size {x, y} of the Board
   * @member
   * @type {object}
   */
  get size () { return { x: this.x, y: this.y } }

  /** get x size of the Board
   * @member
   * @type {number}
   */
  get x () { return this._x }

  /** get y size of the Board
   * @member
   * @type {number}
   */
  get y () { return this._y }
}
