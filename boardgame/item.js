import Wrapable from './wrapable'

/** Class represents a Board Item
 * @extends Wrapable
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class Item extends Wrapable {
  /** Initializes Item */
  init () { this.trigger('init') }

  /** Destroys Item */
  destroy () { this.trigger('destroy') }

  /** get and set custom name of Item
   * @type {string}
   */
  get name () { return this._name }
  set name (v) {
    if (typeof v !== 'string') throw TypeError(`${this.constructor.name}: name not String`)
    this._name = v
    this.trigger('rename', v)
  }

  /** get x and y position (> 0, > 0) of Item
   * @type {object}
   */
  get position () { return { x: this.x, y: this.y } }

  /** get x position (> 0) of Item
   * @type {number}
   */
  get x () { return this._x }

  /** get y position (> 0) of Item
   * @type {number}
   */
  get y () { return this._y }
}
