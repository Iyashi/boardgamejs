import Wrapable from './wrapable'

/** Class represents a game Player
 * @extends Wrapable
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export default class Player extends Wrapable {
  /** get and set custom name of Player
   * @type {string}
   */
  get name () { return this._name }
  set name (v) {
    if (typeof v !== 'string') throw TypeError(`${this.constructor.name}: name not String`)
    this._name = v
    this.trigger('rename', v)
  }
}
