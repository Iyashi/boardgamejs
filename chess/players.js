import * as Boardgame from '../boardgame'
import { PLAYERS } from './constants'

/** Class represents a Chess Player
 * @extends Player
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class ChessPlayer extends Boardgame.Player {
  /** Create Chess Player of given type (color)
   * @constructor
   * @param {number} type Type (color) of Chess Player
   */
  constructor (type) {
    super()
    this.type = type
  }

  /** get and set type (color) of Chess Player
   * @member
   * @type {number}
   */
  get type () { return this._type } // color
  set type (v) { // color
    if (v === PLAYERS.BLACK) this._type = v
    else if (v === PLAYERS.WHITE) this._type = v
    else throw TypeError(`${this.constructor.name}: type not ${PLAYERS.BLACK} (black) or ${PLAYERS.WHITE} (white)`)
  }

  /** get type (color) of Chess Player as string
   * @member
   * @type {string}
   */
  get typeStr () { return this.type === PLAYERS.BLACK ? 'black' : 'white' }
}

/** Class represents a Black Chess Player
 * @extends ChessPlayer
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class Black extends ChessPlayer {
  /** Create Black Chess Player
   * @constructor
   */
  constructor () {
    super(PLAYERS.BLACK)
  }
}

/** Class represents a White Chess Player
 * @extends ChessPlayer
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class White extends ChessPlayer {
  /** Create White Chess Player
   * @constructor
   */
  constructor () {
    super(PLAYERS.WHITE)
  }
}
