import * as Boardgame from '../boardgame'
import { ChessPlayer, Black, White } from './players'
import { FIGURES } from './constants'

/** Class represents a Chess Figure
 * @extends MovableItem
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class ChessFigure extends Boardgame.MovableItem {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White}
   * @param {FIGURES} type One of {@link FIGURES}
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, type, x, y) {
    // initiate base map
    const map = []
    for (let i = 0; i < 15; i++) {
      map[i] = []
      for (let j = 0; j < 15; j++) map[i][j] = Boardgame.MOVES.NOT
    }
    super(map)
    this.player = player
    this.type = type
    if (typeof x !== 'number' || x <= 0 || x > 8) throw TypeError(`${this.constructor.name}: x not Number<1-8>`)
    this._x = x
    if (typeof y !== 'number' || y <= 0 || y > 8) throw TypeError(`${this.constructor.name}: y not Number<1-8>`)
    this._y = y
  }

  /** get part of movement map (8x8) based on current {@link MovableItem#position}
   * @member
   * @type {array<array<MOVES>>}
   */
  get currentMap () { return this.map.slice(8 - this.y, 8 - this.y + 8).map($ => $.slice(8 - this.x, 8 - this.x + 8)) }

  /** get and set {@link ChessPlayer} of {@link ChessFigure}
   * @member
   * @type {ChessPlayer}
   */
  get player () { return this._player }
  set player (v) {
    if (!(v instanceof ChessPlayer)) throw TypeError(`${this.constructor.name}: player not Chess Player`)
    this._player = v
  }

  /** get and set type of {@link ChessFigure}
   * @member
   * @type {FIGURES}
   */
  get type () { return this._type }
  set type (v) {
    for (let FIGURE of Object.values(FIGURES)) if (v === FIGURE) { this._type = v; return }
    throw Error(`${this.constructor.name}: type not valid`)
  }

  /** get type of {@link ChessFigure} as {@link string}
   * @member
   * @type {string}
   */
  get typeStr () {
    if (this.type & FIGURES.KING) return 'king'
    if (this.type & FIGURES.QUEEN) return 'queen'
    if (this.type & FIGURES.BISHOP) return 'bishop'
    if (this.type & FIGURES.KNIGHT) return 'knight'
    if (this.type & FIGURES.ROOK) return 'rook'
    if (this.type & FIGURES.PAWN) return 'pawn'
    return 'invalid'
  }
}

/** Class represents a Chess King Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class King extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.KING, x, y)
    this.setMapDirections(Boardgame.DIRECTIONS.V | Boardgame.DIRECTIONS.H | Boardgame.DIRECTIONS.D, Boardgame.MOVES.WALK_EMPTY | Boardgame.MOVES.WALK_ENEMY, 1)
  }
}

/** Class represents a Chess Queen Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class Queen extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.QUEEN, x, y)
    this.setMapDirections(Boardgame.DIRECTIONS.V | Boardgame.DIRECTIONS.H | Boardgame.DIRECTIONS.D, Boardgame.MOVES.WALK_EMPTY | Boardgame.MOVES.WALK_ENEMY)
  }
}

/** Class represents a Chess Bishop Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class Bishop extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.BISHOP, x, y)
    this.setMapDirections(Boardgame.DIRECTIONS.D, Boardgame.MOVES.WALK_EMPTY | Boardgame.MOVES.WALK_ENEMY)
  }
}

/** Class represents a Chess Knight Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class Knight extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.KNIGHT, x, y)
    this.map[7 - 1][7 - 2] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 - 1][7 + 2] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 + 1][7 - 2] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 + 1][7 + 2] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 - 2][7 - 1] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 + 2][7 - 1] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 - 2][7 + 1] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
    this.map[7 + 2][7 + 1] = Boardgame.MOVES.JUMP_EMPTY | Boardgame.MOVES.JUMP_ENEMY
  }
}

/** Class represents a Chess Rook Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class Rook extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.ROOK, x, y)
    this.setMapDirections(Boardgame.DIRECTIONS.H | Boardgame.DIRECTIONS.V, Boardgame.MOVES.WALK_EMPTY | Boardgame.MOVES.WALK_ENEMY)
  }
}

/** Class represents a Chess Pawn Figure
 * @extends ChessFigure
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 * @todo en passant
 */
export class Pawn extends ChessFigure {
  /**
   * @constructor
   * @param {ChessPlayer} player Instance of {@link Black} or {@link White} Player
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (player, x, y) {
    super(player, FIGURES.PAWN, x, y)
    this.setMapDirections(this.player instanceof Black ? Boardgame.DIRECTIONS.S : Boardgame.DIRECTIONS.N, Boardgame.MOVES.WALK_EMPTY, 2)
    const m = this.player instanceof Black ? 1 : -1
    this.map[7 + 1 * m][7 + 1] = Boardgame.MOVES.WALK_ENEMY
    this.map[7 + 1 * m][7 - 1] = Boardgame.MOVES.WALK_ENEMY
    this.addListener('move', event => {
      if (this.type - FIGURES.PAWN === 0) this.map[7 + 2 * m][7] = Boardgame.MOVES.NOT
      if ((this.player instanceof Black && event.y === 8) || (this.player instanceof White && event.y === 1)) {
        if (this.type - FIGURES.PAWN === 0) this.trigger('start-promote')
      }
    })
  }

  /**
   * Promote {@link Pawn} to given {@link ChessFigure}
   * @param {ChessFigure} figure Instance or Class of {@link ChessFigure} to promote to
   */
  promote (figure) {
    figure = [Queen, Bishop, Knight, Rook].filter($ => figure instanceof $ || figure === $)[0]
    if (!figure) throw Error(`${this.constructor.name}: Chess Figure not one of Queen,Bishop,Knight,Rook`)
    // eslint-disable-next-line new-cap
    if (!(figure instanceof ChessFigure)) figure = new figure(this.player, this.x, this.y)
    this.type += figure.type
    this.map = figure.map
    this.trigger('promote', { target: figure })
  }
}
