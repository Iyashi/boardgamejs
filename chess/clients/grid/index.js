/** Chess class derivates (work-in-progress)
 * @module Boardgames/Chess/Grid
 * @version 1.0.0
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 * @todo implement classes instead of events
 */

import { Wrapable } from '../../../boardgame'
import * as Chessgame from '../../'

// webpack file-loader
import './chess.otf'
import './main.scss'

/** Chess Client: CSS3-Grid
 * @extends Wrapable
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class ChessGrid extends Wrapable {
  /** Create Board
   * @constructor
   * @param {HTMLElement} [parent=document.body] Parent HTML Element to add Game to
   */
  constructor (parent = document.body) {
    super()
    /** Item being hovered or null
     * @member
     * @type {Item}
     */
    this.hoverItem = null

    /** ChessFigure being moved or null
     * @member
     * @type {ChessFigure}
     */
    this.movingFigure = null

    /** Array of Chess Tiles {@link movingFigure} was dragged over
     * @member
     * @type {array<ChessTile>}
     */
    this.movingPath = []

    /** Chess Board Wrapper Object
     * @member
     * @type {HTMLElement}
     */
    this.object = document.createElement('chess-game')

    /** Chess Board used for gameplay
     * @member
     * @type {ChessBoard}
     */
    this.board = new Chessgame.ChessBoard()
    this.board.init()
    this.board.on('init', ({ source }) => {
      this.board.object = document.createElement('chess-board')
      for (let tile of this.board.tiles) {
        tile.on('init', ({ source }) => {
          const elm = document.createElement('chess-tile')
          elm.classList.add(tile.type ? 'white' : 'black')
          elm.addEventListener('dragover', event => {
            event.preventDefault()
            this.dragover(this.movingFigure, tile)
            return false
          }, false)
          elm.addEventListener('drop', event => {
            event.preventDefault()
            this.drop(this.movingFigure, tile)
          }, false)
          tile.object = elm
          this.board.object.appendChild(elm)
        })
        tile.on('destroy', ({ source }) => this.board.object.removeChild(source.object))
      }
      for (let figure of this.board.figures) {
        figure.on('init', ({ source }) => {
          const elm = document.createElement('chess-figure')
          elm.classList.add(figure.player instanceof Chessgame.Black ? 'black' : 'white', figure.typeStr.toLocaleLowerCase())
          elm.setAttribute('draggable', true)
          elm.addEventListener('dragstart', event => {
            event.dataTransfer.setData('text/plain', 'drag')
            this.drag(figure)
          }, false)
          elm.addEventListener('mouseover', event => this.mouseover(figure), false)
          elm.addEventListener('mouseleave', event => this.mouseleave(figure), false)
          figure.object = elm
          this.board.getItems(figure.x, figure.y)[0].object.appendChild(figure.object)
        })
        figure.on('destroy', ({ source }) => this.board.getItems(source.x, source.y)[0].object.removeChild(source.object))
        figure.on('move', ({ source, x, y }) => this.board.getItems(x, y)[0].object.appendChild(source.object))
        if (figure instanceof Chessgame.Pawn) {
          figure.on('start-promote', ({ source }) => this.startPromote(source))
          figure.on('promote', ({ source, target }) => this.promote(source, target))
        }
      }
      this.object.appendChild(this.board.object)
    })
    // this.board.on('destroy', ({ source }) => this.object.removeChild(source.object))
    parent.appendChild(this.object)
  }

  /** Event of dragging Chess Figure
   * @param {ChessFigure} figure Chess Figure being dragged
   */
  drag (figure) {
    if (this.paused) return false
    this.movingFigure = figure
    this.movingPath = []
    this.trigger('drag', { figure })
  }

  /** Event of dragging Chess Figure over Chess Tile
   * @param {ChessFigure} figure Chess Figure being dragged
   * @param {ChessTile} tile Chess Tile being dragged over
   */
  dragover (figure, tile) {
    if (this.paused) return false
    if (this.movingPath[this.movingPath.length - 1] !== tile) {
      const last = this.movingPath.slice(-1)[0]
      if (last) last.object.classList.remove('drag-over')
      this.movingPath.push(tile)
      tile.object.classList.add('drag-over')
      this.trigger('dragover', { figure, target: tile })
    }
  }

  /** Event of dragging Chess Figure over Chess Tile
   * @param {ChessFigure} figure Chess Figure being dropped
   * @param {ChessTile} tile Chess Tile being dropped to
   */
  drop (figure, tile) {
    if (this.paused) return false
    if (tile && figure && this.board.moveFigure(figure, tile)) {
      tile.object.appendChild(figure.object)
    }
    const last = this.movingPath.slice(-1)[0]
    if (last) last.object.classList.remove('drag-over')
    this.movingFigure = null
    this.movingPath = null
    this.mouseover(figure)
    this.trigger('drop', { figure, target: tile })
  }

  /** Event of mouse hovering a Chess Board Item
   * @param {Item} item Item being hovered by mouse
   */
  mouseover (item) {
    this.board.object.querySelectorAll('.move-allow, .move-forbid').forEach($ => $.classList.remove('move-allow', 'move-forbid'))
    this.hoverItem = item
    const paths = this.board.getPaths(item)
    for (let path of paths) {
      for (let step of path.steps) {
        step.valid && step.items[0].object.classList.add('move-allow')
        !step.valid && step.items[0].object.classList.add('move-forbid')
      }
    }
    this.trigger('mouseover', { item })
  }

  /** Event of mouse leaving a Chess Board Item
   * @param {Item} item Item being left by mouse
   */
  mouseleave (item) {
    this.board.object.querySelectorAll('.move-allow, .move-forbid').forEach($ => $.classList.remove('move-allow', 'move-forbid'))
    if (this.hoverItem) {
      this.hoverItem = null
      this.trigger('mouseleave', { item })
    }
  }

  /** Event of starting Chess Figure Promotion (pawn only)
   * @param {ChessFigure} figure Chess Figure being promoted
   */
  startPromote (figure) {
    this.figureSelect = document.createElement('chess-figure-select')
    for (let cls of [Chessgame.Queen, Chessgame.Bishop, Chessgame.Knight, Chessgame.Rook]) {
      const elm = document.createElement('chess-figure')
      elm.classList.add(figure.player ? 'white' : 'black', cls.name.toLocaleLowerCase())
      elm.addEventListener('click', event => figure.promote(cls))
      this.figureSelect.appendChild(elm)
    }
    this.object.appendChild(this.figureSelect)
    this.paused = true
  }

  /** Event of promotion of a Chess Figure (pawn only)
   * @param {ChessFigure} figure Chess Figure being promoted
   * @param {ChessFigure} target Chess Figure to promote to
   */
  promote (figure, target) {
    if (target) {
      figure.object.classList.add(target.typeStr)
      this.object.removeChild(this.figureSelect)
    }
    this.figureSelect = null
    this.paused = false
  }
}
