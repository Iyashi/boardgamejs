
/** Object contains all Chess Player IDs
 * @name PLAYERS
 * @constant {object}
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export const PLAYERS = {
  /** Black Chess Player ID
   * @enum
   */
  BLACK: 0b0,

  /** White Chess Player ID
   * @enum
   */
  WHITE: 0b1
}

/** Object contains all Chess Figure IDs
 * @name FIGURES
 * @constant {object}
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export const FIGURES = {
  /** Chess King ID
   * @enum
   */
  KING: 0b100000,

  /** Chess Queen ID
   * @enum
   */
  QUEEN: 0b010000,

  /** Chess Bishop ID
   * @enum
   */
  BISHOP: 0b001000,

  /** Chess Knight ID
   * @enum
   */
  KNIGHT: 0b000100,

  /** Chess Rook ID
   * @enum
   */
  ROOK: 0b000010,

  /** Chess Pawn ID
   * @enum
   */
  PAWN: 0b000001,

  /** Chess Pawn promoted to Chess Queen ID
   * @enum
   */
  PAWN_QUEEN: 0b010001,

  /** Chess Pawn promoted to Chess Bishop ID
   * @enum
   */
  PAWN_BISHOP: 0b001001,

  /** Chess Pawn promoted to Chess Knight ID
   * @enum
   */
  PAWN_KNIGHT: 0b000101,

  /** Chess Pawn promoted to Chess Rook ID
   * @enum
   */
  PAWN_ROOK: 0b000011
}
