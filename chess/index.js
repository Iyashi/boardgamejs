/** Chess derivates of Boardgame classes
 * @module Boardgames/Chess
 * @version 1.0.0
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 * @todo chess: check & checkmate & stalemate
 * @todo chess: en passant
 */

import { PLAYERS, FIGURES } from './constants'
import { ChessBoard, ChessTile } from './board'
import { ChessPlayer, Black, White } from './players'
import { ChessFigure, King, Queen, Bishop, Knight, Rook, Pawn } from './figures'

const Players = { Black, White }
const Figures = { King, Queen, Bishop, Knight, Rook, Pawn }

export { ChessBoard, ChessTile, PLAYERS, Players, ChessPlayer, Black, White, FIGURES, Figures, ChessFigure, King, Queen, Bishop, Knight, Rook, Pawn }
