import * as Boardgame from '../boardgame'
import { PLAYERS } from './constants'
import { Black, White } from './players'
import { ChessFigure, King, Queen, Bishop, Knight, Rook, Pawn } from './figures'

/** Class represents a Chess Board
 * @extends Board
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 * @todo add History
 * @todo check & checkmate & stalemate
 */
export class ChessBoard extends Boardgame.Board {
  /**
   * @constructor
   * @param {Black} black Black Chess Player
   * @param {White} white White Chess Player
   */
  constructor (black = new Black(), white = new White()) {
    super(8, 8)
    if (!(black instanceof Black)) TypeError(`${this.constructor.name}: black player not Chess Player Black`)
    if (!(white instanceof White)) TypeError(`${this.constructor.name}: white player not Chess Player White`)
    this._players = { black, white }
    this._currentPlayer = this.players.black
  }

  /** Init Chess Board ({@link ChessTile}s and {@link ChessFigure}s) */
  init () {
    // tiles
    for (let y = 1; y <= 8; y++) {
      for (let x = 1; x <= 8; x++) {
        const tile = new ChessTile(x, y)
        this.add(tile)
        tile.init()
      }
    }
    // figures
    for (let player of Object.values(this.players)) {
      let y = player.type ? 8 : 1
      let figures = [
        new King(player, 5, y), new Queen(player, 4, y),
        new Bishop(player, 3, y), new Bishop(player, 6, y),
        new Knight(player, 2, y), new Knight(player, 7, y),
        new Rook(player, 1, y), new Rook(player, 8, y)
      ]
      y = player.type ? 7 : 2
      for (let i = 1; i <= 8; i++) figures.push(new Pawn(player, i, y))
      this.add(...figures)
      for (let figure of figures) figure.init()
    }
    this.trigger('init')
  }

  /** Move {@link ChessFigure} to {@link ChessTile}
   * @param {ChessFigure} figure {@link ChessFigure} to move
   * @param {ChessTile} tile {@link ChessTile} to move to
   * @return {bool} Successfully moved
   */
  moveFigure (figure, tile) {
    if (!(figure instanceof ChessFigure)) throw TypeError('Not ChessFigure')
    if (!(tile instanceof ChessTile)) throw TypeError('Not ChessTile')
    const path = this.getPath(figure, tile.x, tile.y)
    if (typeof path === 'undefined') this.validatePath(path)
    if (!path.valid) return false
    // player turn
    if (this.currentPlayer !== figure.player) return false
    // @TODO: check
    // @TODO: checkmate
    // @TODO: stalemate
    // castling
    const diff = tile.x - figure.x
    if (figure instanceof King && !figure.hasMoved && Math.abs(diff) === 1 && tile.y === figure.y) {
      const last = diff < 0 ? 1 : 8
      for (let i = figure.x + diff; i !== diff + last; diff < 0 ? i-- : i++) {
        const items = this.getItems(i, figure.y)
        if (i !== last && items[1] && (!(items[1] instanceof Rook) || items[1].hasMoved)) {
          break
        } else if (i === last && items[1] instanceof Rook && !items[1].hasMoved) {
          items[1].move(figure.x, figure.y)
        }
      }
    }
    // move each path step
    for (let i = 0; i < path.steps.length; i++) {
      const step = path.steps[i]
      if (step.items.length > 1 && step.items[1].player !== path.item.player) {
        this.remove(step.items[1])
        step.items[1].destroy()
      }
      figure.move(tile.x, tile.y)
    }
    this._currentPlayer = figure.player === this.players.black ? this.players.white : this.players.black
    return true
  }

  /** Add new Items to {@link ChessBoard}
   * @param {...Item} items {@link ChessFigure}s and {@link ChessTile}s to add
   */
  add (...items) {
    for (let item of items) {
      if (!(item instanceof ChessTile) && !(item instanceof ChessFigure)) throw TypeError(`${this.constructor.name}: item not ChessTile or ChessFigure`)
      Boardgame.Board.prototype.add.call(this, item)
    }
  }

  /** get Object of {@link ChessPlayer}s on the Board
   * @member
   * @type {object}
   */
  get players () { return this._players }

  /** get {@link ChessPlayer} of current turn
   * @member
   * @type {ChessPlayer}
   */
  get currentPlayer () { return this._currentPlayer }

  /** get Array of {@link ChessTile}s on {@link ChessBoard}
   * @member
   * @type {array<ChessTile>}
   */
  get tiles () { return this.items.filter($ => $ instanceof ChessTile) }

  /** get Array of {@link ChessFigure}s on {@link ChessBoard}
   * @member
   * @type {array<ChessFigure>}
   */
  get figures () { return this.items.filter($ => $ instanceof ChessFigure) }
}

/** Class represents a Chess Board (ground) Tile
 * @extends StaticItem
 * @author Jannik Lohaus <jannik.lohaus@gmail.com>
 */
export class ChessTile extends Boardgame.StaticItem {
  /**
   * @constructor
   * @param {number} x x position (1-8)
   * @param {number} y y position (1-8)
   */
  constructor (x, y) {
    super(x, y)
    if (typeof x !== 'number' || x <= 0 || x > 8) throw TypeError(`${this.constructor.name}: x not Number<1-8>`)
    if (typeof y !== 'number' || y <= 0 || y > 8) throw TypeError(`${this.constructor.name}: y not Number<1-8>`)
    // Get type: black/white
    this.type = ((this.x - 1) % 2 + (this.y - 1) % 2) % 2 ? PLAYERS.BLACK : PLAYERS.WHITE
  }

  /** get and set type (color) of the {@link ChessTile}
   * @member
   * @type {PLAYERS}
   */
  get type () { return this._type } // color
  set type (v) { // color
    for (let PLAYER of Object.values(PLAYERS)) if (v === PLAYER) { this._type = v; return }
    throw Error(`${this.constructor.name}: type not valid`)
  }
}
