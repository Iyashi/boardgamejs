'use strict'

const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

const rslv = (...steps) => path.resolve(__dirname, ...steps)

const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development'
const server = process.env.SERVER === 'true'

const stats = {
  all: false,
  timings: true,
  builtAt: !server || mode === 'production',
  warnings: !server || mode === 'production',
  errors: !server || mode === 'production',
  assets: !server || mode === 'production'
}

const watchOptions = { poll: 500, aggregateTimeout: 1000 }

module.exports = {
  mode,
  entry: {
    'engine': rslv('boardgame/index.js'),
    'chess/engine': [
      rslv('boardgame/index.js'),
      rslv('chess/index.js')
    ],
    'chess/grid': [
      rslv('boardgame/index.js'),
      rslv('chess/index.js'),
      rslv('chess/clients/grid/index.js')
    ],
    'games': [
      rslv('boardgame/index.js'),
      rslv('chess/index.js'),
      rslv('chess/clients/grid/index.js'),
      rslv('index.js')
    ]
  },
  output: {
    path: rslv('dist/'),
    publicPath: '',
    filename: `[name].js`,
    sourceMapFilename: 'source-map/[name].js'
  },
  module: {
    rules: [
      { test: /\.js$/i,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: { formatter: require('eslint-formatter-friendly') }
      },
      { test: /\.(sa|sc|c)ss$/i,
        exclude: /node_modules/,
        use: [
          { loader: server ? 'style-loader' : MiniCssExtractPlugin.loader },
          { loader: 'css-loader', options: { importLoaders: 1, sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      { test: name => /\.(typeface\.json|ttf|otf|eot|woff2?)$/.test(name) && /(chess|three\/examples\/fonts)\//.test(name),
        type: 'javascript/auto',
        loader: 'file-loader',
        options: { name: 'chess/fonts/[name].[ext]' }
      }
    ]
  },
  resolve: {
    modules: [rslv('node_modules/'), rslv('boardgame/'), rslv('chess/')],
    extensions: ['.js', ',json']
  },
  performance: { hints: false },
  devtool: mode === 'production' ? 'hidden-source-map' : 'source-map',
  target: 'web',
  stats,
  plugins: [
    new HTMLWebpackPlugin({ template: rslv('index.html') }),
    new MiniCssExtractPlugin({ filename: `[name].css`, chunkFilename: `[id].css` })
  ],
  optimization: {
    nodeEnv: mode,
    minimize: mode === 'production',
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
        uglifyOptions: {
          compress: true,
          mangle: true,
          keep_classnames: true,
          keep_fnames: true
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  devServer: {
    clientLogLevel: 'none',
    contentBase: rslv('dist/'),
    host: '127.0.0.1',
    port: 8080,
    overlay: { errors: true, warnings: true },
    stats,
    watchOptions
  },
  watchOptions
}
