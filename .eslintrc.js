'use strict'

module.exports = {
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2018,
    ecmaFeatures: {
      impliedStrict: true
    }
  },
  env: {
    es6: true,
    browser: true,
    node: true
  },
  plugins: [
    'node',
    'html',
    'standard',
    'import',
    'promise',
    'compat',
    'security'
  ],
  extends: [
    'eslint:recommended',
    'standard',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:promise/recommended',
    'plugin:compat/recommended',
    'plugin:security/recommended'
  ],
  settings: {
    polyfills: ['promises'] // TODO: polyfills (eg. babel)
  },
  rules: {
    'no-debugger': 'off',
    'no-console': 'off',
    'security/detect-object-injection': 'off'
  }
}
